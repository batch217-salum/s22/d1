// console.log("Hello World");

// [Section] array methods

// Mutator method

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruits"];

// push()

console.log("Current array:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log("Mutated array from push method:");
console.log(fruits);

// Adding multiple elements to array
fruits.push("avocado", "guava");
console.log("Mutated array from push method:");
console.log(fruits);

// pop()

let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);

// unshift()

fruits.unshift("Lime", "Banana");
console.log("Mutated array from unshift method:");
console.log(fruits);

// shift()
let anotherFruit = fruits.shift();
console.log("Mutated array from shift method:");
console.log(fruits);

// splice()

fruits.splice(1, 2, "Lime", "Cherry");
console.log("Mutated array from splice method:");
console.log(fruits);

// sort()

fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);


// reverse()

fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);


// Non-mutator methods

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];

// indexOf()

let firstIndex = countries.indexOf("PH");
console.log("Result of indexOf Method: " +firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of indexOf Method: " + invalidCountry);

// lastIndexOf()

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf Method: " + lastIndex);

let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf Method: " + lastIndexStart);


//slice()

let slicedArrayA = countries.slice(2);
console.log("Result from slice method:");
console.log(slicedArrayA);

//slice another index

let slicedArrayB = countries.slice(2, 4);
console.log("Result from slice method:");
console.log(slicedArrayB);

let slicedArrayC = countries.slice(-3);
console.log("Result from slice method:");
console.log(slicedArrayC);

// toString()

let stringArray = countries.toString();
console.log("Result from toString method");
console.log(stringArray);

// concat()

let taskArrayA = ["drink html", "eat javascript"];
let taskArrayB = ["inhale css", "breathe sass"];
let taskArrayC = ["get git", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method");
console.log(tasks);

// combining multiple arrays

console.log("Result from concat method");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// combing arrays with elements

let combinedTasks = taskArrayA.concat("smell express", "throw react");
console.log("Result from concat method");
console.log(combinedTasks);


// join()

let users = ["John", "Jane", "Joe", "Robert"];
console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));
console.log(users.join(' ! '));

// Iteration Methods

// forEach
allTasks.forEach(function(task){
	console.log(task);
});

let filteredTasks = [];

allTasks.forEach(function(task){

	if(task.length > 10){
		filteredTasks.push(task);
	}
})

console.log("Result of filtered task: ");
console.log(filteredTasks);


let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
})
console.log("Original Array:");
console.log(numbers); //original is unnaffected by map();
console.log("Result of map method:");
console.log(numberMap);


//map() vs forEach()
let numberForEach = numbers.forEach(function(number){
	return number * number;
})

console.log(numberForEach); //undefined result

// every()

let allValid = numbers.every(function(number){
	return (number > 2);
})

console.log("Result of every method:");
console.log(allValid);


// some()

let someValid = numbers.some(function(number){
	return (number < 3);
});

console.log("Result of some method:");
console.log(someValid);

if(someValid){
	console.log("Some numbers in the array are greater than 3");
}


// filter()

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result of filter method:");
console.log(filterValid);


// No element found
let nothingFound = numbers.filter(function(number){
	return (number = 0);
})
console.log("Result of filter method:");
console.log(nothingFound);

// filtering forEach

let filteredNumbers = [];

numbers.forEach(function(number){
	if(number < 3){
		filteredNumbers.push(number);
	}
})

console.log("Result of filter method:");
console.log(filteredNumbers);


// includes()

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);


// reduce()

let iteration = 0;

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("CurrentValue: " + y);

	return x + y;
});

console.log("Result of reduct methord: " + reducedArray);